$(document).ready(function () {
    password.bindEvents();
    post.bindEvents();
    post.refresh();
});

/******** Send posts **************************/
function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    return date + ' ' + month + ' ' + year + ' ' + minTwoDigits(hour) + ':' + minTwoDigits(min) + ':' + minTwoDigits(sec);
}
function minTwoDigits(n) {
    return (n < 10 ? '0' : '') + n;
}
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

var post = {
    bindEvents: function () {
        var self = this;
        $('#showPostBox').on('click', function (e) {
            self.ajaxCheckCaptcha();
            self.showPostBox();
        });
        $('#closePostBox').on('click', this.closePostBox);
        $('#refresh').on('click', function (e) {
            self.refresh();
        });
    },
    closePostBox: function () {
        console.log("check");
        $('#postFormBox').fadeOut();
        $('#content').removeClass('addOverlay');
    },
    showPostBox: function () {
        $('#postFormBox').fadeIn();
        $('#content').addClass('addOverlay');
    },
    sendForm: function () {
        this.ajaxPost();
    },
    refresh: function () {
        this.ajaxGetPosts();
    },
    ajaxCheckCaptcha: function () {
        $.ajax({
            type: 'POST',
            url: '/api/captcha',
            dataType: 'json'
        }).done(function (config) {
            if (config[0] === true) {
                $('.g-recaptcha').removeClass('hidden');
            } else {
                $('.g-recaptcha').addClass('hidden');
            }
        }).fail(function (xhr, message, error) {
            console.log("ERROR " + error);
        });
    },
    showPosts: function (posts) {
        $output = '';
        for (var i = 0; i < posts.length; i++) {
            $output += '<div class="post">'
                + '<h3>' + posts[i].title + '</h3>'
                + '<img src="' + htmlEntities(posts[i].image_url !== ''?posts[i].image_url : '/assets/img/noImage.png'  ) + '" />'
                + '<p class="message">' + posts[i].message + '</p>'
                + '<p class="info">By user: ' + htmlEntities(posts[i].username + ' posted on ' + timeConverter(posts[i].date)) + '</p>'
                + '</div>';
        }

        $('#overviewOfPosts').empty().append($output);
    },
    ajaxGetPosts: function () {
        var self = this;
        $.ajax({
            type: 'POST',
            url: '/api/getPosts',
            dataType: 'json'
        })
            .done(function (posts) {
                self.showPosts(posts);
            })
            .fail(function (xhr, message, error) {
                console.log("ERROR " + error);
            });
    }
};
var password = {
    bindEvents: function () {
        $('#register #retypePassword,#register #password').on('keyup', this.verification)
    },
    verification: function () {
        var password = $('#password').val();
        var verifyPassword = $("#retypePassword").val();

        console.log('valid');

        $('#password').removeClass();
        $('#retypePassword').removeClass();
        if (password === verifyPassword) {
            $('#password').addClass('dataValid');
            $('#retypePassword').addClass('dataValid');
        } else {
            $('#password').addClass('dataInvalid');
            $('#retypePassword').addClass('dataInvalid');
        }

    }
};