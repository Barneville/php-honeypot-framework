<?php
/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert and Dieter Tinel
 * Date: 4/11/2015
 * Time: 22:32
 */

/**
 * @param $value
 * @return string
 */

function fakeXSSv2($value)
{
    $value = strtolower($value);
    $value = htmlEncode($value);
    if (preg_match('/script/', $value)) {
        $value = preg_replace("/[^A-Za-z ]/", '', $value);
        $value = str_replace(array(
            'lt', 'gt', 'script', 'alert'
        ), '', $value);
        $message = htmlEncode($value);
        $value = "<script>alert(\"Some hacker posted the following message: $message\")</script>";
    }
    return $value;
}

/*function fakeXSS($value)
{
    /* Replace all < and > characters by &lt; and &gt;. */
//$value = preg_replace('/<([^<>]*)>/i', '&lt;${1}&gt;', $value);

/* Allow the <script>alert(message)</script> to trap skiddies. */
/*$value = preg_replace(
    '/&lt;scr[il]pt[ ]?[^&]*&gt;[^&]*alert\( *["\']([^&<>\'"]*)["\'] *\)[^&]*(&lt;\/scr[il]pt[ ]?[^&]*&gt;)?/i',
    '"><script>alert(\'Some hacker posted the following message: ${1}\')</script>', $value);*/

/* Replace all <script> tags by <scrlpt>. Creates confusion because the tags looks the same. */
//$value = preg_replace('/&lt;(\/?scr)i(pt)[ ]?[^&]*&gt;/i', '<${1}l${2}>', $value);

/* Remove all tags except script tags. Creates confusion because the tags disappear inside input fields. */
//$value = preg_replace('/&lt;[^&]*&gt;/i', '', $value);

/* Print changed value to the output. */
//return $value;
//}*/

function fakePostsXSS($posts)
{
    $newPosts = array();
    foreach ($posts as $post) {
        $post = (array)$post;
        if ($post['username'] === auth()->getLoggedUser()->getUsername()) {
            $post['title'] = fakeXSSv2($post['title']);
            $post['message'] = fakeXSSv2($post['message']);
        } else {
            $post['title'] = htmlEncode($post['title']);
            $post['message'] = htmlEncode($post['message']);
        }
        $newPosts[] = $post;
    }
    return $newPosts;
}

function fakeSQLInjection($value)
{
    if (preg_match('/.*\'.*/', $value)) {
        response()->write('
<br /><b>Fatal error</b>:  Uncaught exception \'PDOException\' with message \'SQLSTATE[HY000] [2002] Incorrect syntax near \'4\'
Unclosed quotation mark after the character string "SELECT * FROM users WHERE username = \'admin\' AND password = \'fcvc5679se21".]
\' in C:\websites\honeypot-group06\www\Database.php:31
Stack trace:
#0 C:\websites\honeypot-group06\www\Database.php(31): PDO-&gt;__construct(\'mysql:host=localhost\', \'honeypot\', \'JkoNb49x4x5S1\')');
    }
}

function returnFalsyRPHP()
{
    /* This function returns random RDZ PHP code to user */
    $rand = rand(1, 3);

    return view('/honeypot/rdz' . $rand . '.php');
}