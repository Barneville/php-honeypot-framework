public function getId()
{
return $this->id;
}

/**
* @param mixed $id
*/
public function setId($id)
{
$this->id = $id;
}

/**
* @return mixed
*/
public function getTitle()
{
return $this->title;
}

/**
* @param mixed $title
*/
public function setTitle($title)
{
throwError(self::validateTitle($title));
$this->title = $title;
}