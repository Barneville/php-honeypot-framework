<div class="box" id="register">
    <h2>Register</h2>
    <form action="/index/register" method="post">
        <label for="username">Username:</label>
        <input type="text" name="username" pattern=".{2,25}" required title="2-25 characters" value="<?php sout(request()->username) ?>" /><br>
        <label for="name">Fullname:</label>
        <input type="text" name="name" id="name" pattern=".{2,50}" required title="2-50 characters" value="<?php sout(request()->name) ?>" /><br>
        <label for="email">E-mail:</label>
        <input type="email" name="email" id="email" required="required" value="<?php sout(request()->email) ?>" /><br>
        <label for="password">Password:</label>
        <input type="password" name="password" id="password" required="required" /><br>
        <label for="retypePassword">Retype password:</label>
        <input type="password" name="retypePassword" id="retypePassword" />
        <div class="g-recaptcha" data-sitekey="6LcBZRATAAAAAO1yRtCOZoNUxaxbc3QFGa9NnHv-"></div>
        <div id="error"><?php sout(content()->error); ?></div>
        <input type="submit" value="Register"/>
    </form>
</div>