<div id="overview">
    <div id="title">
        <h2>Welcome to the overview... post a message or have a look</h2>
    </div>
    <div id="overviewOfPosts">
    </div>
    <div class="buttons">
        <a href="/index/logout?token=<?php sout(auth()->getExpectedToken()); ?>" class="button">Logout</a>
        <a href="#" class="button" id="refresh">Refresh</a>
        <a href="#" class="button" id="showPostBox">Post</a>
    </div>

    <div class="box hidden" id="postFormBox">

        <form action="/index/post" method="post" enctype="multipart/form-data" ?>
            <div id="closePostBox" class="exitButton"></div>
            <h2>Say something</h2>
            <label for="postTitle">Title:</label>
            <input type="text" name="postTitle" id="postTitle" pattern=".{2,30}" required title="2-30 characters"/>
            <label for="message">Message:</label>
            <textarea name="message" id="message" maxlength="180" required title="0-180 characters"></textarea>
            <label for="image">Image</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="<?php sout(config()->uploads['maxSize'])?>" />
            <input name="image" type="file" accept="image/*" />
            <div class="g-recaptcha hidden" data-sitekey="6LcBZRATAAAAAO1yRtCOZoNUxaxbc3QFGa9NnHv-"></div>
            <input type="submit" value="Post"/>
        </form>
    </div>
</div>