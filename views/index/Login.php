<div id="login" class="box">
    <h2>Login</h2>

    <form action="/index/login" method="post">
        <label for="username">Username:</label>
        <input type="text" name="username" required = "required" pattern=".{2,25}" title="2-25 characters" value="<?php sout(request()->username) ?>"/><br>

        <label for="password">Password:</label>
        <input type="password" name="password" id="password" required="required"/><br>
        <?php
        if (auth()->isLoginCaptchaRequired())
            out('<div class="g-recaptcha" data-sitekey="6LcBZRATAAAAAO1yRtCOZoNUxaxbc3QFGa9NnHv-"></div>');
        ?>
        <div id="error"><?php sout(content()->error); ?></div>
        <input type="submit" value="Login"/>
    </form>
</div>