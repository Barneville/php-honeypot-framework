<!DOCTYPE html>

<html lang="en">

<head>
    <title>DROP3SCAP&#8364</title>
    <meta charset="utf-8"/>
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="cleartype" content="on">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
    <link type="text/css" rel="stylesheet" href="/assets/css/common.css"/>
</head>

<body>
<header>
    <h1 class="logo"><a href="/index/home">DROP3SCAP&#8364</a></h1>
</header>

<main>
    <?php render(content()->main); ?>
</main>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="/assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/assets/js/common.js"></script>

</body>

<footer>
    <p>Made by Group06 - Dieter Tinel, Wouter Bloeyaert, Steven Verscheure </p>
</footer>
<div id="otherGroups">
    <a href="/index/others">Other groups</a>
</div>
</html>