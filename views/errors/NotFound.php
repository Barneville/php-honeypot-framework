<?php

$messages = array(

    array(
        'I think you are on the wrong page.',
        'What did you expect to find here?',
        'Welcome to this very useless page.',
        'Have you ever heard about 404?',
        'Trust me, there is nothing special here.',
        'Where is the party? Not here in any case!',
    ),

    array(
        'You really need a good GPS.',
        'And you\'re on the wrong page again.',
        'Scientific research shows that some people quickly lost their way.',
        'Maybe rebooting your computer will help you to find the right page.',
        'Also visit the following not existing page: /beeabee',
        'A donkey doesn\'t bump twice to the same url... euh stone.',
    ),

    array(
        'Why? Is our home page maybe not interesting enough?',
        'I am beginning to think you\'re trying to do something strange.',
        'I don\'t want to disturb you but there is more in the world than 404!',
        'Do you mean this: https://www.youtube.com/watch?v=ogY44aX5pHU',
        'Kaantaa taman tekstin Google kaantaa ymmartaa sita.',
        'Just when I discovered the meaning of life, they changed it.',
    ),

    array(
        'Oh, i know it! You are searching the admin page isn\'t it? Here it is: /management/admin',
    ),

    array(
        'Ever heard of irony?',
    ),

    array(
        'You really never give up i see.',
    ),

    array(
        'I will stop talking.',
    ),

    array(
        'This are my last words.',
    ),

    array(
        'This are my very last words.',
    ),

    array(
        'This are my very very last words.',
    ),

    array(
        'This are my very very very last words.',
    ),

    array(
        'This are my very very very very last words.',
    ),

    array(
        '"And then there was silence."',
    ),

    array(
        '...',
    ),

    array(
        'Error 404: Page not found',
    ),
);


$message_nr = session()->notFoundMsgNr % count($messages);
$message = $messages[$message_nr];

if ($message_nr < count($messages) - 1)
    session()->notFoundMsgNr++;

sout($message[rand(0, count($message) - 1)]);