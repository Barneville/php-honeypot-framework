<?php

use dropEscape\core\Config;

Config::setGlobal(array(

    'debugMode' => false,

    'defaultPath' => 'index',

    'faviconPath' => 'assets/img/favicon.ico',

    'maxUserInactivePeriod' => 60 * 60, // 1 hour

    'db' => array(
        'host' => 'localhost',
        'username' => 'dropescape',
        'password' => 'oq9s9wGmfoq9BYYWjGKnb8ZCPex7Pp',
        'database' => 'dropescape',
    ),

    'loginAttempts' => array(
        'untilCaptcha' => 3,
        'untilBlock' => 10,
        'blockPeriod' => 5 * 60, // 5 minutes
    ),

    'posts' => array(
        'timePerPost' => 30,
    ),

    'uploads' => array(
        'maxSize' => 5242880,
        'allowedMimes' => array(
            'image/gif' => '.gif',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'image/bmp' => '.bmp',
        ),
    ),

    'assets' => array(
        'allowedMimes' => array(
            'text/css',
            'application/javascript',
            'image/x-icon',
            'image/gif',
            'image/jpeg',
            'image/png',
            'image/bmp',
            'image/svg+xml'
        ),
    ),

    'characters' => array(
        'alpha_low' => 'abcdefghijklmnopqrstuvwxyz',
        'alpha_cap' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        'digits' => '0123456789',
        'special_name' => '-\' ',
        'special_username' => '-_',
    ),

    'constraints' => array(
        'name_min_length' => 1,
        'name_max_length' => 50,
        'username_min_length' => 2,
        'username_max_length' => 25,
        'email_min_length' => 0,
        'email_max_length' => 30,
        'password_min_length' => 0,
        'password_max_length' => 60,
        'post_message_min_length' => 0,
        'post_message_max_length' => 180,
        'post_title_min_length' => 2,
        'post_title_max_length' => 30
    ),

    'errors' => array(
        0x0000 => 'Success',
        0x0001 => 'Username is invalid.',
        0x0002 => 'Name is invalid.',
        0x0003 => 'Email is invalid.',
        0x0004 => 'Password is invalid.',
        0x0005 => 'Wrong username or password.',
        0x0006 => 'Username already exists.',
        0x0007 => 'The captcha is invalid.',
        0x0008 => 'The user is not signed in.',
        0x0009 => 'Invalid token',
        0x000A => 'The user is already signed in.',
        0x000B => 'Your account has expired.',
        0x000C => 'Your account has been blocked for some time.',
        0x000D => 'Too many login attempts. Your account has been blocked for some time.',
        0x0011 => 'The post title is invalid.',
        0x0012 => 'The message is invalid',
        0x0013 => 'User is not authenticated',
        0x0014 => 'File is to large',
        0x0015 => 'File is not an image',
        0x0016 => 'The file is invalid.',
    ),

    'reCaptchaSecret' => '6LcBZRATAAAAAL2LlUIcLmBkIf_B49yyKGEMs_Zn',
));