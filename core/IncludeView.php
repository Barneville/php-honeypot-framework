<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 3/11/2015
 * Time: 0:51
 */

namespace dropEscape\core;

class IncludeView extends View
{
    private $file;

    /**
     * DelegateView constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Checks if the file to render exists.
     */
    public function existsFile()
    {
        return file_exists($this->file);
    }

    /**
     * Handles the include view.
     */
    protected function onHandle()
    {
        ob_start();
        include $this->file;
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }
}