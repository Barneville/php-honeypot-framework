<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 3/11/2015
 * Time: 0:51
 */

namespace dropEscape\core;

class ContentView extends View
{
    private $file;

    /**
     * DelegateView constructor.
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

   /**
     * Checks if the file to render exists.
     */
    public function existsFile()
    {
        return file_exists($this->file);
    }

    /**
     * Gets the file mime type.
     */
    public function getMimeType()
    {
        $info = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($info, $this->file);
        $extension = pathinfo($this->file, PATHINFO_EXTENSION);
        switch ($extension) {
            case 'css':
                $mimeType = 'text/css';
                break;
            case 'js':
                $mimeType = 'application/javascript';
            default:
                break;
        }
        return $mimeType;
    }

    /**
     * Renders the content view.
     */
    protected function onRender()
    {
        response()->setHeader('Content-Type', $this->getMimeType());
        parent::onRender();
    }

    /**
     * Handles the content view.
     */
    protected function onHandle()
    {
        return file_get_contents($this->file);
    }
}