<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 2/11/2015
 * Time: 17:40
 */

namespace dropEscape\core;

class Config {

    private static $globalConfig;
    private $settings;

    /**
     * Config constructor.
     * @param array $settings
     */
    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    /**
     * Sets the global configuration.
     * @param array $settings
     */
    public static function setGlobal($settings)
    {
        self::$globalConfig = new Config($settings);
    }

    /**
     * Gets the global configuration.
     */
    public static function getGlobal()
    {
        return self::$globalConfig;
    }

    /**
     * Gets a setting.
     * @param $property
     * @return mixed
     */
    public function __get($property) {
        return $this->settings[$property];
    }
}