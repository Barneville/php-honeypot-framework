<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 2/11/2015
 * Time: 18:21
 */

namespace dropEscape\core;

use dropEscape\controllers\AssetsController;

class WebApp implements Handler, Rendable
{
    /**
     * Includes the php config file.
     * @param string $configFile The php config file to include.
     */
    public function run($configFile)
    {
        include $configFile;

        if (Config::getGlobal()->debugMode) {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }

        $this->render();
        response()->send();
    }

    /**
     * Renders HTTP output.
     */
    public function render()
    {
        $result = $this->handle();

        if (empty($result)) {
            $this->renderNull();
        } elseif (!($result instanceof Rendable)) {
            response()->setHeader('Content-Type', 'application/json');
        }

        render($result);
    }

    /**
     * @return string
     */
    private function renderNull()
    {
        response()->setStatusCode(Response::STATUS_NOT_FOUND);
        ?>
        <html>
        <head>
            <title>Page not found</title>
        </head>
        <body>
        <h1>404 Not Found</h1>
        The page that you have requested could not be found.
        </body>
        </html>
        <?php
    }

    /**
     * Handles the request
     * @return mixed
     */
    public function handle()
    {
        if($this->isFaviconPath())
            return $this->handleFavicon();

        $config = Config::getGlobal();
        $controller = $this->createController(request());
        if (empty($controller)) {
            $tempRequest = new Request();
            $defPath = Request::cleanPath($config->defaultPath);
            $tempRequest->setPath($defPath . '/' . request()->getPath());
            $controller = $this->createController($tempRequest);
            if (empty($controller)) {
                return null;
            } else {
                request()->setPath($tempRequest->getPath());
            }
        }
        return $controller->handle();
    }

    private function isFaviconPath()
    {
        return request()->getPath() === 'favicon.ico';
    }

    private function handleFavicon()
    {
        request()->setPath(config()->faviconPath);
        return (new AssetsController())->handle();
    }

    /**
     * Creates the controllers.
     * @param Request $request
     * @return controller
     */
    private function createController(Request $request)
    {
        $path = $request->getControllerPath();
        return $this->createControllerByPath($path);
    }

    /**
     * Creates the controllers.
     * @param $path
     * @return controller
     */
    private function createControllerByPath($path)
    {
        $className = 'dropEscape\\controllers\\'
            . ucfirst($path) . 'Controller';
        $validClass = canLoadClass($className)
            && class_exists($className);
        return $validClass ? new $className : null;
    }
}