<?php

/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert and Dieter Tinel
 * Date: 3/11/2015
 * Time: 13:57
 */

use dropEscape\core\Authentication;
use dropEscape\core\Config;
use dropEscape\core\Content;
use dropEscape\core\ContentView;
use dropEscape\core\Error;
use dropEscape\core\IncludeView;
use dropEscape\core\Handler;
use dropEscape\core\RedirectView;
use dropEscape\core\Rendable;
use dropEscape\core\Request;
use dropEscape\core\Response;
use dropEscape\core\Session;
use dropEscape\core\UploadException;
use dropEscape\services\DropEscapeDao;

/**
 * Alias for creating an IncludeView.
 * @param $viewFile
 * @return IncludeView
 */
function view($viewFile)
{
    $filePath = 'views' . DIRECTORY_SEPARATOR . $viewFile;
    return new IncludeView($filePath);
}

/**
 * Alias for creating a ContentView for an asset.
 * @param $assetFile
 * @return IncludeView
 */
function asset($assetFile)
{
    $assetFile = str_replace('/', DIRECTORY_SEPARATOR, $assetFile);
    $filePath = 'assets' . DIRECTORY_SEPARATOR . $assetFile;
    return new ContentView($filePath);
}

/**
 * Alias for creating a ContentView for an uploaded file.
 * @param $assetFile
 * @return IncludeView
 */
function uploadedFile($assetFile)
{
    $assetFile = str_replace('/', DIRECTORY_SEPARATOR, $assetFile);
    $filePath = 'uploads' . DIRECTORY_SEPARATOR . $assetFile;
    return new ContentView($filePath);
}

/**
 * Uploads a file and returns the url of the file.
 */
function uploadFile($file)
{
    if ($file['error']) {
        throw new UploadException($file['error']);
    }

    $tmpName = $file['tmp_name'];
    $info = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($info, $tmpName);
    $validMimeTypes = config()->uploads['allowedMimes'];

    if ($file['size'] > config()->uploads['maxSize']) {
        throw new Error(0x0014);
    } elseif (!array_key_exists($mime, $validMimeTypes)) {
        throw new Error(0x0015);
    }

    $extension = $validMimeTypes[$mime];
    $newName = generateUniqueFilename() . $extension;
    $newPath = 'uploads' . DIRECTORY_SEPARATOR . $newName;
    move_uploaded_file($tmpName, $newPath);
    return '/uploads/' . $newName;
}

/**
 * Alias for creating a RedirectView.
 * @param $url
 * @return RedirectView
 */
function redirection($url)
{
    return new RedirectView($url);
}

/**
 * Handles the object.
 * @param $obj
 * @return mixed
 */
function handle($obj)
{
    if ($obj instanceof Handler)
        return $obj->handle();
    else
        return $obj;
}

/**
 * Renders the data to HTTP output.
 * @param $data
 * @return string
 */
function render($data)
{
    if ($data instanceof Rendable)
        $data->render();
    else
        out(json_encode($data));
}

/**
 * Convert all applicable characters to HTML entities.
 * @param string $value
 * @return string
 */
function htmlEncode($value)
{
    return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

/**
 * Prints to the output.
 * @param string $value
 */
function out($value)
{
    echo $value;
}

/**
 * Prints securely to the output.
 * @param string $value
 */
function sout($value)
{
    out(htmlEncode($value));
}

/**
 * Alias for getting the global request instance.
 * @return Request
 */
function request()
{
    return Request::getGlobal();
}

/**
 * Alias for getting the global response instance.
 * @return Response
 */
function response()
{
    return Response::getGlobal();
}

/**
 * Alias for getting the global session instance.
 * @return Session
 */
function session()
{
    return Session::getGlobal();
}


/**
 * Alias for getting the global content instance.
 * @return Content
 */
function content()
{
    return Content::getGlobal();
}

/**
 * Alias for getting the global config instance.
 * @return Config
 */
function config()
{
    return Config::getGlobal();
}

/**
 * Alias for getting the global authentication instance.
 * @return Authentication
 */
function auth()
{
    return Authentication::getGlobal();
}

/**
 * From StackOverflow:
 * http://stackoverflow.com/questions/18206851/com-create-guid-function-got-error-on-server-side-but-works-fine-in-local-usin
 * @return string
 */
function generateGUID()
{
    if (function_exists('com_create_guid')) {
        return com_create_guid();
    } else {
        mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
        $charId = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            . substr($charId, 0, 8) . $hyphen
            . substr($charId, 8, 4) . $hyphen
            . substr($charId, 12, 4) . $hyphen
            . substr($charId, 16, 4) . $hyphen
            . substr($charId, 20, 12)
            . chr(125);// "}"
        return $uuid;
    }
}

function generateUniqueFilename()
{
    mt_srand((double)microtime() * 10000);
    $charId = strtoupper(md5(uniqid(rand(), true)));
    $uuid = substr($charId, 0, 8)
        . substr($charId, 8, 4)
        . substr($charId, 12, 4)
        . substr($charId, 16, 4)
        . substr($charId, 20, 12);
    return $uuid;
}

/**
 * @param $password
 * @return string
 */
function createBCryptPasswordHash($password)
{
    return password_hash($password, PASSWORD_BCRYPT);
}

/**
 * @param $value
 * @param $allowedChars
 * @return bool
 */
function isValid($value, $allowedChars)
{
    for ($i = 0; $i < strlen($value); $i++) {
        if (strpos($allowedChars, $value[$i]) === false)
            return false;
    }
    return true;
}

/**
 * @return bool
 */
function checkIfEmpty()
{
    $count = count(func_get_args());
    $isTrue = false;
    for ($i = 0; $i < $count; $i++) {
        if (empty(func_get_arg($i))) $isTrue = true;
    }
    return $isTrue;
}

/**
 * @param $errorCode
 * @throws Error
 */
function throwError($errorCode)
{
    if ($errorCode !== 0x0000)
        throw new Error($errorCode);
}

/**
 * @return bool
 */
function checkCaptcha()
{
    $arrContextOptions = array(   //TODO: Some little nasty SSL bug
        "ssl" => array(
            "verify_peer" => false,
            "verify_peer_name" => false,
        ),
    );

    $response = file_get_contents(
        'https://www.google.com/recaptcha/api/siteverify?'
        . 'secret=' . config()->reCaptchaSecret
        . '&response=' . request()->{'g-recaptcha-response'}
        . '&remoteip=' . request()->getServerParam('REMOTE_ADDR')
        , false, stream_context_create($arrContextOptions));

    return json_decode($response)->success;
}

/**
 * @param DropEscapeDao $dao
 * @return bool
 */
function isMessagePostCaptchaRequired(DropEscapeDao $dao)
{
    $timeOfSecondLatestQuery = $dao->getTimeOfLatestFormInsertWithOffset(auth()->getLoggedUser()->getId(),1)->date;
    $timeOfLatestQuery = $dao->getTimeOfLatestFormInsertWithOffset(auth()->getLoggedUser()->getId(),0)->date;

    if($timeOfLatestQuery === null || $timeOfLatestQuery===null){
        return false;
    }

    $interval = $timeOfLatestQuery - $timeOfSecondLatestQuery;
    return ($interval <= config()->posts['timePerPost']);
}