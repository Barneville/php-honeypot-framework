<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 8/11/2015
 * Time: 19:41
 */

namespace dropEscape\core;

class RedirectView extends View
{
    private $url;

    /**
     * DelegateView constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Handles the include view.
     */
    protected function onHandle()
    {
        return true;
    }

    /**
     * Renders the view.
     */
    protected function onRender()
    {
        response()->setHeader('Location', $this->url);
        sout("Redirecting ...");
    }
}