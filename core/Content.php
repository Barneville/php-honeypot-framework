<?php

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 6/11/2015
 * Time: 14:02
 */

namespace dropEscape\core;

class Content
{
    private static $global;
    private $data;

    /**
     * Content constructor.
     */
    public function __construct()
    {
        $this->data = array();
    }

    /**
     * @return Content
     */
    public static function getGlobal()
    {
        if (!isset(self::$global)) {
            self::$global = new Content();
        }
        return self::$global;
    }

    /**
     * Get content data.
     * @param string $property
     * @return string Property value
     */
    public function __get($property)
    {
        if (!isset($this->data[$property]))
            return null;

        return $this->data[$property];
    }

    /**
     * Set content data.
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        $this->data[$property] = $value;
    }
}