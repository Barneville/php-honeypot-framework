<?php

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 2/11/2015
 * Time: 20:01
 */

namespace dropEscape\core;

use ReflectionMethod;

abstract class Controller implements Handler
{
    /**
     * Handles the controller.
     * @return View Returns the view.
     */
    public final function handle()
    {
        return $this->onHandle();
    }

    /**
     * Is invoked when the controller is handled.
     * @return View Returns the view.
     */
    protected function onHandle()
    {
        if (empty(trim(request()->getFunctionPath()))) {
            return $this->onHandleDefault();
        } else {
            $result = $this->handleFunction();
            if (empty($result))
                $result = $this->onHandleNotFound();
            return $result;
        }
    }

    /**
     * Is invoked when the function path is empty.
     */
    protected function onHandleDefault()
    {
        return null;
    }

    /**
     * Is invoked when the function is not found.
     */
    protected function onHandleNotFound()
    {
        return null;
    }

    /**
     * Invokes the right function.
     * @return mixed
     */
    private function handleFunction()
    {
        $method = $this->getMethod();

        if ($method === null)
            return null;

        $args = $this->createArgs($method);

        if($args === null)
            return "Missing parameters";

        return $method->invokeArgs($this, $args);
    }

    /**
     * @return null|ReflectionMethod
     */
    private function getMethod()
    {
        $httpMethod = $this->getCleanHTTPMethodName() . '_';
        $functionName = $httpMethod . request()->getFunctionPath();

        if (method_exists('dropEscape\core\Controller', $functionName))
            return null;

        if (!method_exists($this, $functionName))
            return null;

        $reflection = new ReflectionMethod($this, $functionName);
        return $reflection->isPublic() ? $reflection : null;
    }

    /**
     * @param $method
     * @return array|null
     */
    private function createArgs($method)
    {
        $params = array();
        $httpMethod = strtolower(request()->getServerParam('REQUEST_METHOD'));
        $httpArgs = ($httpMethod === 'get')
            ? request()->getQueryData()
            : request()->getPostData();

        foreach ($method->getParameters() as $param) {
            if (!isset($httpArgs[$param->getName()])) {
                return null;
            }
            $params[] = $httpArgs[$param->getName()];
        }

        return $params;
    }

    /**
     * Checks if the method is public.
     * @param $methodName
     * @return bool
     */
    private function isMethodPublic($methodName)
    {
        $reflection = new ReflectionMethod($this, $methodName);
        return $reflection->isPublic();
    }

    /**
     * @param $httpHethod
     */
    private function getCleanHTTPMethodName()
    {
        $httpMethod = request()->getServerParam('REQUEST_METHOD');
        $httpMethod = preg_replace('/[^a-z]/i', '', $httpMethod);
        return strtolower($httpMethod);
    }
}