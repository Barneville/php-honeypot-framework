<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 2/11/2015
 * Time: 23:48
 */

namespace dropEscape\core;

abstract class View implements Handler, Rendable
{
    /**
     * Renders the view.
     */
    public final function render()
    {
        $this->onRender();
    }

    /**
     * Renders the view.
     */
    protected function onRender()
    {
        out(strval($this->handle()));
    }

    /**
     * Handles the view.
     * @return mixed
     */
    public function handle()
    {
        return $this->onHandle();
    }

    /**
     * Handles the view.
     * @return mixed
     */
    protected function onHandle()
    {
    }
}