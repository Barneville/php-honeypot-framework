<?php

namespace dropEscape\core;
use Exception;

/**
 * Created by PhpStorm.
 * User: Dieter
 * Date: 8/11/2015
 * Time: 15:00
 */
class Error extends Exception
{
    /**
     * Error constructor.
     * @param string $code
     */
    public function __construct($code)
    {
        parent::__construct(
            config()->errors[$code],
            $code);
    }
}