<?php

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 4/11/2015
 * Time: 14:44
 */

namespace dropEscape\core;

interface Handler
{
    /**
     * Handles the object instance.
     * @return mixed
     */
    function handle();
}