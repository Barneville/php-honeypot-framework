<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 3/11/2015
 * Time: 0:52
 */

namespace dropEscape\core;

use Exception;
use IteratorAggregate;
use Traversable;

class Request
{
    private static $global;
    private $path;
    private $controllerPath;
    private $functionPath;
    private $actionPath;
    private $queryData;
    private $postData;
    private $cookieData;
    private $serverData;
    private $fileData;

    /**
     * Request constructor.
     * @param null $queryData
     * @param null $postData
     * @param $cookieData
     * @param $serverData
     * @param null $fileData
     */
    public function __construct($queryData = null, $postData = null,
                                $cookieData = null, $serverData = null,
                                $fileData = null)
    {
        $this->queryData = $queryData ? $queryData : $_GET;
        $this->postData = $postData ? $postData : $_POST;
        $this->cookieData = $cookieData ? $cookieData : $_COOKIE;
        $this->serverData = $serverData ? $serverData : $_SERVER;
        $this->fileData = $fileData ? $fileData : $_FILES;
        $this->setPath($this->q);
    }

    /**
     * @return Request
     */
    public static function getGlobal()
    {
        if (!isset(self::$global)) {
            self::$global = new Request();
        }
        return self::$global;
    }

    /**
     * @return array
     */
    public function getQueryData()
    {
        return $this->queryData;
    }

    /**
     * @return array
     */
    public function getPostData()
    {
        return $this->postData;
    }

    /**
     * @return array
     */
    public function getCookieData()
    {
        return $this->cookieData;
    }

    /**
     * @return array
     */
    public function getServerData()
    {
        return $this->serverData;
    }

    /**
     * @return array
     */
    public function getFileData()
    {
        return $this->fileData;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getQueryParam($key)
    {
        if (!isset($this->queryData[$key]))
            return null;
        return $this->queryData[$key];
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getPostParam($key)
    {
        if (!isset($this->postData[$key]))
            return null;
        return $this->postData[$key];
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getCookie($key)
    {
        if (!isset($this->cookieData[$key]))
            return null;
        return $this->cookieData[$key];
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getServerParam($key)
    {
        if (!isset($this->serverData[$key]))
            return null;
        return $this->serverData[$key];
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getFile($key)
    {
        if (!isset($this->fileData[$key]))
            return null;
        return $this->fileData[$key];
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $this->cleanPath($path);
        $pieces = explode('/', $this->path);
        $this->controllerPath = $pieces[0];
        if (count($pieces) > 1)
            $this->functionPath = $pieces[1];
        unset($pieces[0]);
        $this->actionPath = implode('/', $pieces);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getControllerPath()
    {
        return $this->controllerPath;
    }

    /**
     * @return string
     */
    public function getFunctionPath()
    {
        return $this->functionPath;
    }

    /**
     * @return string
     */
    public function getActionPath()
    {
        return $this->actionPath;
    }

    /**
     * Get request data.
     * @param string $property
     * @return string Property value
     */
    public function __get($property)
    {
        if (isset($this->queryData[$property]))
            return $this->queryData[$property];

        if (isset($this->postData[$property]))
            return $this->postData[$property];

        if (isset($this->cookieData[$property]))
            return $this->cookieData[$property];

        return null;
    }

    /**
     * @param $property
     * @param $value
     * @throws Exception
     */
    public function __set($property, $value)
    {
        throw new Exception("The request is readonly.");
    }

    /**
     * Cleans a path.
     * @param string $path
     * @return string The cleaned path.
     */
    public static function cleanPath($path)
    {
        if (empty($path)) return '';
        $path = preg_replace(array(
            '/[^a-zA-Z0-9_$\-\!\.\*\'\(\)\,\/ ]/',
            '/[\/]+/',
            '/[.]+/',
        ), array('', '/', '.'), $path);
        $path = trim($path, " \t\n\r\0\x0B/.");
        return strtolower($path);
    }
}