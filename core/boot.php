<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 3/11/2015
 * Time: 3:48
 */

function classNameToFile($className)
{
    $slices = explode('\\', $className);
    unset($slices[0]);
    $filename = implode(DIRECTORY_SEPARATOR, $slices);
    return $filename . ".php";
}

function canLoadClass($className)
{
    $filename = classNameToFile($className);
    return file_exists($filename);
}

function __autoload($className)
{
    $filename = classNameToFile($className);
    include_once($filename);
}

include_once 'Utils.php';