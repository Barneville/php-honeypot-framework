<?php

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 4/11/2015
 * Time: 20:50
 */

namespace dropEscape\core;

use Exception;

class Response
{
    const STATUS_NOT_FOUND = 404;

    private static $global;
    private $statusCode;
    private $headers;
    private $cookies;
    private $messageBody;

    /**
     * Response constructor.
     */
    private function __construct()
    {
        $this->statusCode = 200;
        $this->headers = array();
        $this->cookies = array();
        $this->messageBody = "";
        ob_start();
    }

    /**
     * @return Request
     */
    public static function getGlobal()
    {
        if (!isset(self::$global)) {
            self::$global = new Response();
        }
        return self::$global;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = intval($statusCode);
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $name
     * @param $value
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * @param $name
     */
    public function getHeader($name)
    {
        return $this->headers[$name];
    }

    /**
     * @param string $name
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $domain
     * @param bool|false $secure
     * @param bool|true $httponly
     */
    public function setCookie($name, $value = "", $expire = 0, $path = "",
                              $domain = "", $secure = false, $httponly = true)
    {
        $this->cookies[$name] = array(
            'value' => $value,
            'expire' => $expire,
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure,
            'httponly' => $httponly,
        );
    }

    /**
     * @param $name
     * @return array
     */
    public function getCookie($name)
    {
        return $this->cookies[$name];
    }

    /**
     * @param $value
     */
    public function write($value)
    {
        $this->messageBody .= $value;
    }

    /**
     * @return mixed
     */
    public function getMessageBody()
    {
        return $this->messageBody;
    }

    /**
     * Sends the HTTP output.
     */
    public function send()
    {
        $this->sendStatusCode();
        $this->sendHeaders();
        $this->sendCookies();
        $this->sendMessageBody();
    }

    /**
     * Sends the status code.
     */
    private function sendStatusCode()
    {
        http_response_code($this->statusCode);
    }

    /**
     * Sends the HTTP headers to the output.
     */
    private function sendHeaders()
    {
        foreach ($this->headers as $name => $value) {
            header($name . ': ' . $value);
        }
    }

    /**
     * Sends the cookies to the output.
     */
    private function sendCookies()
    {
        foreach ($this->cookies as $name => $cookie) {
            setcookie($name,
                $cookie['value'],
                $cookie['expire'],
                $cookie['path'],
                $cookie['domain'],
                $cookie['secure'],
                $cookie['httponly']
            );
        }
    }

    /**
     * @param $property
     * @param $value
     * @throws Exception
     */
    public function __set($property, $value)
    {
        throw new Exception("You cannot set custom properties.");
    }

    /**
     * Sends the HTTP message body to the output.
     */
    private function sendMessageBody()
    {
        echo $this->messageBody;
    }

    /**
     * @return string
     */
    private function sendOutputBuffer()
    {
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
    }
}