<?php

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 6/11/2015
 * Time: 13:52
 */

namespace dropEscape\core;

class Session
{
    /**
     * This class is responsible for session management, an extra layer of security is added with a cookie token.
     */
    private static $global;

    /**
     * Session constructor.
     */
    private function __construct()
    {
        ini_set('session.cookie_httponly', true);
        ini_set('session.use_only_cookies', true);
        ini_set('session.gc_maxlifetime',
            config()->maxUserInactivePeriod);
        $this->load();
    }

    /**
     * @return Session
     */
    public static function getGlobal()
    {
        if (!isset(self::$global)) {
            self::$global = new Session();
        }
        return self::$global;
    }

    /**
     * Get session data.
     * @param string $property
     * @return string Property value
     */
    public function __get($property)
    {
        if (!isset($_SESSION[$property]))
            return null;

        return $_SESSION[$property];
    }

    /**
     * Set session data.
     * @param $property
     * @param $value
     */
    public function __set($property, $value)
    {
        $_SESSION[$property] = $value;
    }

    /**
     * Remove all session variables.
     */
    public function clear()
    {
        session_unset();
    }

    /**
     * Destroys the session.
     */
    public function destroy()
    {
        session_destroy();
    }

    /**
     * Loads the session.
     */
    private function load()
    {
        session_name('sid');
        session_start();
    }
}