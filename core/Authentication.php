<?php

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 8/11/2015
 * Time: 18:57
 */

namespace dropEscape\core;

use dropEscape\models\User;
use dropEscape\services\DropEscapeDao;

class Authentication
{

    private static $global;
    private $loggedUser;
    private $dao;

    /**
     * Authentication constructor.
     * @param $loggedUser
     */
    public function __construct()
    {
        register_shutdown_function(array($this, 'finalize'));
        $dbh = Database::createDatabaseHandler();
        $this->dao = new DropEscapeDao($dbh);
        $this->loginCaptchaRequired = false;
        $this->loggedUser = null;
        $this->loadUser();
    }

    /**
     * Finalizes the authentication.
     */
    public function finalize()
    {
        $this->updateUser();
    }

    /**
     * @return Authentication
     */
    public static function getGlobal()
    {
        if (!isset(self::$global)) {
            self::$global = new Authentication();
        }
        return self::$global;
    }

    /**
     * Gets the signed in user.
     * @return User
     */
    public function getLoggedUser()
    {
        return $this->loggedUser;
    }

    /**
     * Logs the user in.
     * @param $username
     * @param $password
     * @return int
     */
    public function login($username, $password)
    {
        if ($this->isLogged())
            return 0x000A;

        $user = $this->dao->getUserByUsername($username);
        $maxAttempts = config()->loginAttempts['untilCaptcha'];
        session()->loginCaptchaRequired = false;

        if (empty($user))
            return 0x0005;

        if ($user->getLoginAttempts() > $maxAttempts && !checkCaptcha()) {
            session()->loginCaptchaRequired = true;
            return 0x0007;
        }

        $error = $user->login($password);
        $this->dao->storeUser($user);

        if ($user->getLoginAttempts() > $maxAttempts)
            session()->loginCaptchaRequired = true;

        if ($error !== 0x0000)
            return $error;

        session()->loginCaptchaRequired = false;
        session()->loggedUserId = $user->getId();
        session()->token = $this->generateToken();
        $ip = request()->getServerParam('REMOTE_ADDR');
        $user->setIpAddress($ip);
        $this->loggedUser = $user;
        return 0x0000;
    }

    /**
     * Registers the user.
     * @param $name
     * @param $email
     * @param $password
     * @param $username
     * @return int|mixed
     */
    public function register($name, $email, $password, $checkPassword, $username)
    {
        if ($password !== $checkPassword)
            return 0x0004;

        if (!checkCaptcha())
            return 0x0007;

        if ($this->dao->checkDuplicateUsername($username))
            return 0x0006;

        try {
            $newUser = new User();
            $newUser->setName($name);
            $newUser->setEmail($email);
            $newUser->setPassword($password);
            $newUser->setUsername($username);
            $this->dao->storeUser($newUser);
        } catch (Error $ex) {
            return $ex->getCode();
        }

        return 0x0000;
    }


    /**
     * Logs the user out.
     * @return int
     */
    public function logout()
    {
        if (!$this->isLogged())
            return 0x0008;

        if (!$this->isValidToken())
            return 0x0009;

        session()->loggedUserId = null;
        $this->getLoggedUser()->setExpiryDate(0);
        $this->loggedUser = null;
        session()->destroy();
        return 0x0000;
    }

    /**
     * Checks if the user is signed in.
     * @return bool
     */
    public function isLogged()
    {
        return !empty($this->loggedUser);
    }

    /**
     * @return boolean
     */
    public function isLoginCaptchaRequired()
    {
        return session()->loginCaptchaRequired;
    }

    /**
     * Gets the expected token.
     */
    public function getExpectedToken()
    {
        return session()->token;
    }

    /**
     * Gets the given token.
     */
    public function getGivenToken()
    {
        return request()->getQueryParam('token');
    }

    /**
     * Checks if the given token is valid.
     */
    public function isValidToken()
    {
        return !empty($this->getExpectedToken())
        && $this->getExpectedToken() === $this->getGivenToken();
    }

    /**
     * @return boolean
     */
    public function isUpdateTokenEnabled()
    {
        return $this->updateTokenEnabled;
    }

    /**
     * @param boolean $updateTokenEnabled
     */
    public function setUpdateTokenEnabled($updateTokenEnabled)
    {
        $this->updateTokenEnabled = $updateTokenEnabled;
    }

    /**
     * Loads the logged user.
     */
    private function loadUser()
    {
        $id = session()->loggedUserId;
        if (!empty($id)) {
            $user = $this->dao->getUserById($id);
            $ip = request()->getServerParam('REMOTE_ADDR');

            if (empty($user)
                || time() < $user->getUnlockDate()
                || time() >= $user->getExpiryDate()
                || time() >= $user->getLogoutDate()
                || $user->getIpAddress() !== $ip
            ) return;

            $user->setLogoutDate(time() + config()->maxUserInactivePeriod);
            $this->loggedUser = $user;
        }
    }

    /**
     * Updates the logged user.
     */
    private function updateUser()
    {
        if ($this->isLogged()) {
            $this->dao->storeUser($this->loggedUser);
        }
    }

    /*
     * Generates a token.
     */
    private static function generateToken(){
        $guid = generateGUID();
        return preg_replace('/[^a-z0-9]+/i', '', $guid);
    }
}