<?php

use dropEscape\core\Request;

/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 4/11/2015
 * Time: 14:43
 */

namespace dropEscape\core;

interface Rendable
{
    /**
     * Renders the HTTP output.
     */
    function render();
}