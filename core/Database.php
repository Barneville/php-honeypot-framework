<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 2/11/2015
 * Time: 17:36
 */

namespace dropEscape\core;

use PDO;

class Database
{
    /**
     * Database constructor.
     */
    private function __construct(){
    }

    /**
     * Creates a new PDO database handler.
     * @param Config $config
     * @return PDO Returns a database handler.
     */
    public static function createDatabaseHandler() {
        $host = config()->db['host'];
        $username = config()->db['username'];
        $password = config()->db['password'];
        $database = config()->db['database'];
        $dbh = new PDO("mysql:host=$host; dbname=$database", $username, $password);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbh;
    }
}