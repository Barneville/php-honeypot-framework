<?php
/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert and Dieter Tinel
 * Date: 3/11/2015
 * Time: 19:23
 */

namespace dropEscape\models;

class Post
{
    private $id;
    private $title;
    private $message;
    private $date;
    private $ownerId;
    private $imageUrl;

    /**
     * @param null $id
     * @param null $date
     */
    public function __construct($id = null, $date=null)
    {
        $this->setId( $id ? $id : generateGUID());
        $this->setDate($date ? $date : time());
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        throwError(self::validateTitle($title));
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        throwError(self::validateMessage($message));
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * @param mixed $ownerId
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;
    }

    /**
     * @param $message
     * @return int
     */
    public static function validateMessage($message){

        $valid = (
            strlen($message) >= config()->constraints['post_message_min_length'] &&
            strlen($message) <= config()->constraints['post_message_max_length']);

        return $valid ? 0x0000 : 0x0012;
    }
    public static function validateTitle($title){
        $valid = (
            strlen($title) >= config()->constraints['post_title_min_length'] &&
            strlen($title) <= config()->constraints['post_title_max_length']);

        return $valid ? 0x0000 : 0x0011;
    }
}