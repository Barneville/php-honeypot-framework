<?php
/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert and Dieter Tinel
 * Date: 3/11/2015
 * Time: 19:24
 */

namespace dropEscape\models;

use dropEscape\core\Error;

class User
{
    private $id;
    private $name;
    private $email;
    private $registerDate;
    private $passwordHash;
    private $username;
    private $loginAttempts;
    private $expiryDate;
    private $unlockDate;
    private $logoutDate;
    private $ipAddress;

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->setId($id ? $id : generateGUID());
        $this->setRegisterDate(time());
        $this->setExpiryDate(time() + (365 * 24 * 60 * 60)); // One year
        $this->setUnlockDate(0);
        $this->setLogoutDate(0);
        $this->loginAttempts = 0;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @throws Error
     */
    public function setName($name)
    {
        throwError(self::validateName($name));
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email
     * @throws Error
     */
    public function setEmail($email)
    {
        throwError(self::validateEmail($email));
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param mixed $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param mixed $passwordHash
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @param $password
     */
    public function  setPassword($password)
    {
        throwError(self::validatePassword($password));
        $this->setPasswordHash(createBCryptPasswordHash($password));
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param $username
     * @throws Error
     */
    public function setUsername($username)
    {
        throwError(self::validateUsername($username));
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getLoginAttempts()
    {
        return $this->loginAttempts;
    }

    /**
     * @param int $loginAttempts
     */
    public function setLoginAttempts($loginAttempts)
    {
        $this->loginAttempts = $loginAttempts;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return mixed
     */
    public function getUnlockDate()
    {
        return $this->unlockDate;
    }

    /**
     * @param mixed $unlockDate
     */
    public function setUnlockDate($unlockDate)
    {
        $this->unlockDate = $unlockDate;
    }

    /**
     * @return mixed
     */
    public function getLogoutDate()
    {
        return $this->logoutDate;
    }

    /**
     * @param mixed $logoutDate
     */
    public function setLogoutDate($logoutDate)
    {
        $this->logoutDate = $logoutDate;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param mixed $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @param $password
     * @return int ErrorCode
     */
    public function login($password, $suppressCapthca = false)
    {
        $attemptsConfig = config()->loginAttempts;

        if (time() < $this->unlockDate)
            return 0x000C;

        if (password_verify($password, $this->getPasswordHash())) {
            $this->logoutDate = time() + config()->maxUserInactivePeriod;
            $this->loginAttempts = 0;

            if (time() >= $this->expiryDate)
                return 0x000B;
            else
                return 0x0000;
        }

        if ($this->loginAttempts > $attemptsConfig['untilBlock']) {
            $this->unlockDate = time() + $attemptsConfig['blockPeriod'];
            $this->loginAttempts = 0;
            return 0x000D;
        }

        $this->loginAttempts++;
        return 0x0005;
    }

    /**
     * @param $name
     * @return int
     */
    public static function validateName($name)
    {
        $allowed = config()->characters['alpha_low']
            . config()->characters['alpha_cap']
            . config()->characters['special_name'];

        $valid = (isValid($name, $allowed) &&
            strlen($name) >= config()->constraints['name_min_length'] &&
            strlen($name) <= config()->constraints['name_max_length']);

        return $valid ? 0x0000 : 0x0002;
    }

    /**
     * @param $username
     * @return int
     */
    public static function validateUsername($username)
    {
        $allowed = config()->characters['alpha_low']
            . config()->characters['alpha_cap']
            . config()->characters['digits']
            . config()->characters['special_username'];

        $valid = (isValid($username, $allowed) &&
            strlen($username) >= config()->constraints['name_min_length'] &&
            strlen($username) <= config()->constraints['name_max_length']);

        return $valid ? 0x0000 : 0x0001;
    }

    /**
     * @param $email
     * @return int
     */
    public static function validateEmail($email)
    {
        $valid = (filter_var($email, FILTER_VALIDATE_EMAIL) &&
            strlen($email) >= config()->constraints['name_min_length'] &&
            strlen($email) <= config()->constraints['name_max_length']);

        return $valid ? 0x0000 : 0x0003;
    }

    /**
     * @param $password
     * @return int
     */
    public static function  validatePassword($password)
    {
        $valid = (
            strlen($password) >= config()->constraints['password_min_length'] &&
            strlen($password) <= config()->constraints['password_max_length']);

        return $valid ? 0x0000 : 0x0004;
    }
}