<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 2/11/2015
 * Time: 14:47
 */

use dropEscape\core\WebApp;

require_once 'core/Boot.php';
require_once 'honeypot/Deception.php';

$app = new WebApp();
$app->run('config/default.config.php');