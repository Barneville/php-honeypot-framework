<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 15/11/2015
 * Time: 11:57
 */

namespace dropEscape\controllers;

use dropEscape\core\ContentView;
use dropEscape\core\Controller;

class UploadsController extends Controller
{
    protected function onHandle()
    {
        if (!auth()->isLogged()) {
            content()->main = view('errors/forbidden.php');
            return view('shared/master.php');
        }

        if (empty(request()->getActionPath()))
            return null;

        $upload = uploadedFile(request()->getActionPath());

        if (!$upload->existsFile()) {
            return null;
        } else if (!$this->isAllowedMimeType($upload)) {
            content()->main = view('errors/forbidden.php');
            return view('shared/master.php');
        } else {
            return $upload;
        }
    }

    private function isAllowedMimeType(ContentView $upload)
    {
        $validMimeTypes = config()->uploads['allowedMimes'];
        return array_key_exists($upload->getMimeType(), $validMimeTypes);
    }
}