<?php

/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert and Dieter Tinel
 * Date: 3/11/2015
 * Time: 13:14
 */

namespace dropEscape\controllers;

use dropEscape\core\Controller;
use dropEscape\core\Database;
use dropEscape\core\Error;
use dropEscape\core\UploadException;
use dropEscape\models\post;
use dropEscape\services\DropEscapeDao;
use Exception;

class IndexController extends Controller
{
    private $dao;

    public function __construct()
    {
        $dbh = Database::createDatabaseHandler();
        $this->dao = new DropEscapeDao($dbh);
    }

    protected function onHandle()
    {
        content()->main = parent::onHandle();
        if (preg_match("/.*\.php/", request()->getActionPath()) ){
            return returnFalsyRPHP();
        }else if(request()->getActionPath() === 'fileexplorer'){
            return view('/honeypot/fileExplorer.php');
        }else{
            return view('shared/master.php');
        }
    }

    protected function onHandleDefault()
    {
        return $this->get_home();
    }

    protected function onHandleNotFound()
    {
        return view('errors/notFound.php');
    }

    /* =========================================================================== */
    /* HTTP Routed Methods */
    /* =========================================================================== */
    public function get_admin()
    {
        return view('/honeypot/admin.php');
    }
    public function get_others(){
        return view('/index/Others.php');
    }

    public function get_home()
    {
        if (auth()->isLogged()) {
            return redirection('/index/overview');
        } else {
            return view('/index/home.php');
        }

    }

    public function get_success()
    {
        return view('index/success.php');
    }

    public function get_register()
    {
        if (auth()->isLogged())
            return redirection('/index/overview');
        else
            return view('index/register.php');
    }

    public function post_register($name, $email, $password, $retypePassword, $username)
    {
        $error = auth()->register($name, $email, $password, $retypePassword, $username);

        if ($error !== 0x000)
            content()->error = config()->errors[$error];
        else return $this->createMessageView(
            'Successfully registered',
            'You are now part of our community, enjoy!');

        return view('index/register.php');
    }

    public function get_login()
    {
        if (auth()->isLogged())
            return redirection('/index/overview');
        else
            return view('/index/login.php');
    }

    public function post_login($username, $password)
    {
        if (strtolower($username) === 'admin' && $password === 'fcvc5679se21') {
            return redirection('/index/admin');
        } else {

            fakeSQLInjection($username);
            $error = auth()->login($username, $password);

            if ($error === 0x000)
                return redirection('/index/overview');
            else
                content()->error = config()->errors[$error];

            return view('/index/login.php');
        }

    }

    public function post_post($postTitle, $message)
    {
        $error = $this->post($postTitle, $message);

        if (is_string($error))
            return $this->createMessageView("Oops... something went terrible wrong!", $error);
        else if ($error === 0x000)
            return redirection('/index/overview');
        else
            return $this->createMessageView("Oops... something went terrible wrong!", config()->errors[$error] . "... Return to the main page by clicking on the logo.");
    }

    public function post($title, $message)
    {

        //Check time since last post if lower than enable session captcha required if bigger disable captcha integration.
        //Make api call that tells user if captcha is required...
        //Only check sended captcha value if required.
        //Each time post is called check timeInterval again
        //Display custom error messages in form... via php

        $image = request()->getFile('image');
        $imageUrl = "";

        if (!empty($image['name'])) {
            try {
                $imageUrl = uploadFile($image);
            } catch (Error $error) {
                return $error->getCode();
            } catch (UploadException $ex) {
                return $ex->getMessage();
            }
        }

        if (!auth()->isLogged()) {
            return 0x0008;
        }

        if (isMessagePostCaptchaRequired($this->dao) && !checkCaptcha()) {
            return 0x0007;
        }

        try {
            $post = new Post();
            $post->setMessage($message);
            $post->setOwnerId(auth()->getLoggedUser()->getId());
            $post->setTitle($title);
            $post->setImageUrl($imageUrl);
            $this->dao->storePost($post);
        } catch (Error $ex) {
            return $ex->getCode();
        }
        return 0x0000;
    }

    public function get_overview()
    {
        if (auth()->isLogged()) {
            return view('/index/overview.php');
        } else {
            return redirection('/index/login');
        }
    }

    public function get_logout()
    {
        if (!auth()->isLogged()) {
            return redirection('/index/home');
        }
        $error = auth()->logout();
        if ($error !== 0x0000) {
            return $this->createMessageView(
                'Logout failed',
                config()->errors[$error]);
        } else {
            return $this->createMessageView(
                'Successfully signed out',
                'We miss you already! :\'(');
        }
    }

    /* =========================================================================== */
    /* Private Methods */
    /* =========================================================================== */

    /**
     * @param $title
     * @param $body
     * @return \dropEscape\core\IncludeView
     */
    private function createMessageView($title, $body)
    {
        content()->messageTitle = $title;
        content()->messageBody = $body;
        return view('index/message.php');
    }
}