<?php
/**
 * Created by PhpStorm.
 * User: Dieter Tinel
 * Date: 3/11/2015
 * Time: 11:04
 */

namespace dropEscape\controllers;

use dropEscape\core\ContentView;
use dropEscape\core\Controller;

class AssetsController extends Controller
{
    protected function onHandle()
    {
        if (empty(request()->getActionPath()))
            return null;

        $asset = asset(request()->getActionPath());

        if (!$asset->existsFile()) {
            return null;
        } else if (!$this->isAllowedMimeType($asset)) {
            content()->main = view('errors/forbidden.php');
            return view('shared/master.php');
        } else {
            return $asset;
        }
    }

    private function isAllowedMimeType(ContentView $asset)
    {
        $validMimeTypes = config()->assets['allowedMimes'];
        return in_array($asset->getMimeType(), $validMimeTypes);
    }
}