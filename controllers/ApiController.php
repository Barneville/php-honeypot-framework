<?php

/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert
 * Date: 2/11/2015
 * Time: 20:00
 */

namespace dropEscape\controllers;

use dropEscape\core\Controller;
use dropEscape\core\Database;
use dropEscape\services\DropEscapeDao;

class ApiController extends Controller
{
    private $dao;

    public function __construct()
    {
        $dbh = Database::createDatabaseHandler();
        $this->dao = new DropEscapeDao($dbh);
    }

    protected function onHandle()
    {
        if (auth()->isLogged())
            return parent::onHandle();
        return array('error', config()->errors[0x0013]);
    }

    public function post_captcha()
    {
        return array(isMessagePostCaptchaRequired($this->dao));
    }

    public function post_getPosts()
    {
        return fakePostsXSS($this->getPosts());
    }

    private function getPosts()
    {
        return $this->dao->getPosts(0);
    }

}