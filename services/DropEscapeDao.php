<?php
/**
 * Created by PhpStorm.
 * User: Wouter Bloeyaert and Dieter Tinel
 * Date: 2/11/2015
 * Time: 17:27
 */

namespace dropEscape\services;

use dropEscape\models\Category;
use dropEscape\models\Post;
use dropEscape\models\Topic;
use dropEscape\models\User;
use PDO;

class DropEscapeDao
{
    private $dbh;

    /**
     * MessageDao constructor.
     * @param PDO $dbh Database handler.
     */
    public function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param $id
     * @param $rankOrder
     * @return mixed|null
     */
    public function getTimeOfLatestFormInsertWithOffset($id, $offset)
    {
        $sql = "SELECT m.date
                FROM messages m
                JOIN users u ON u.id = m.user_id
                WHERE u.id = :id
                ORDER BY m.date DESC
                LIMIT 1 OFFSET :amountOfOffset";
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam('id', $id, PDO::PARAM_STR);
        $stmt->bindParam('amountOfOffset', $offset, PDO::PARAM_INT);
        $stmt->execute();
        if ($stmt->rowCount() === 0)
            return null;

        return $result = $stmt->fetch(PDO::FETCH_OBJ);
    }

    /**
     * @return array|null
     */
    public function getPosts()
    {
        $sql = "SELECT m.message, m.title, u.username, m.date, m.image_url
                FROM messages m
                JOIN users u ON u.id = m.user_id
                ORDER BY m.date DESC
                LIMIT 40";
        $stmt = $this->dbh->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0)
            return null;

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }


    /**
     * @param $id
     * @return User
     */
    public function getUserById($id)
    {
        $sql = "SELECT * FROM users WHERE id = :id";
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();

        if ($stmt->rowCount() === 0)
            return null;

        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $user = new User($result->id);
        $user->setName($result->name);
        $user->setEmail($result->email);
        $user->setRegisterDate($result->registerDate);
        $user->setPasswordHash($result->passwordHash);
        $user->setUsername($result->username);
        $user->setLoginAttempts($result->loginAttempts);
        $user->setExpiryDate($result->expiryDate);
        $user->setUnlockDate($result->unlockDate);
        $user->setLogoutDate($result->logoutDate);
        $user->setIpAddress($result->ipAddress);

        return $user;
    }

    /**
     * @param $username
     * @return User
     */
    public function getUserByUsername($username)
    {
        $sql = "SELECT * FROM users WHERE username = :username";
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam('username', $username);
        $stmt->execute();

        if ($stmt->rowCount() === 0)
            return null;

        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $user = new User($result->id);
        $user->setName($result->name);
        $user->setEmail($result->email);
        $user->setRegisterDate($result->registerDate);
        $user->setPasswordHash($result->passwordHash);
        $user->setUsername($result->username);
        $user->setLoginAttempts($result->loginAttempts);
        $user->setExpiryDate($result->expiryDate);
        $user->setUnlockDate($result->unlockDate);
        $user->setLogoutDate($result->logoutDate);
        $user->setIpAddress($result->ipAddress);

        return $user;
    }

    /**
     * @param $username
     * @return bool
     */
    public function checkDuplicateUsername($username)
    {
        $sql = "SELECT id FROM users WHERE username = :username";
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam('username', $username);
        $stmt->execute();

        return $stmt->rowCount() > 0;
    }

    public function checkValidUserId($userId)
    {
        $sql = "SELECT id FROM users WHERE id = :id";
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam('id', $userId);
        $stmt->execute();

        return $stmt->rowCount() > 0;
    }

    /**
     * @param $username
     * @param $password
     * @return bool|null
     */
    public function validateUser($username, $password)
    {
        $sql = "SELECT * FROM users WHERE username= :username";
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam('username', $username);
        $stmt->execute();

        if ($stmt->rowCount() === 0)
            return null;

        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $user = new User($result->id, $result->name, $result->email,
            $result->registerDate, $result->passwordHash, $result->username);

        return password_verify($password, $user->getPasswordHash());
    }

    /**
     * @param Post $post
     */
    public function storePost(Post $post)
    {
        $this->storeObject('messages', 'id', $post->getId(),
            array(
                'message' => $post->getMessage(),
                'date' => $post->getDate(),
                'title' => $post->getTitle(),
                'user_id' => $post->getOwnerId(),
                'image_url' => $post->getImageUrl(),
            ));
    }

    /**
     * @param User $user
     */
    public function storeUser(User $user)
    {
        $this->storeObject('users', 'id', $user->getId(),
            array(
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'registerDate' => $user->getRegisterDate(),
                'passwordHash' => $user->getPasswordHash(),
                'username' => $user->getUsername(),
                'loginAttempts' => $user->getLoginAttempts(),
                'expiryDate' => $user->getExpiryDate(),
                'unlockDate' => $user->getUnlockDate(),
                'logoutDate' => $user->getLogoutDate(),
                'ipAddress' => $user->getIpAddress(),
            ));
    }

    /**
     * @param $table
     * @param $idField
     * @param $id
     * @param $data
     */
    private function storeObject($table, $idField, $id, $data)
    {
        $sql = $this->buildStoreQuery($table, $idField, $data);
        $data[$idField] = $id;
        $stmt = $this->dbh->prepare($sql);
        $stmt->bindParam($idField, $id);
        $stmt->execute($data);
    }

    /**
     * @param $table
     * @param $idField
     * @param $id
     * @param $data
     * @return string
     */
    private function buildStoreQuery($table, $idField, $data)
    {
        $fields = $values = $assignments = $split = '';
        foreach ($data as $key => $value) {
            $fields .= "$split `$key`";
            $values .= "$split :$key";
            $assignments .= "$split `$key`=VALUES(`$key`)";
            $split = ',';
        }
        return "INSERT INTO $table (`$idField`, $fields) VALUES (:$idField, $values) "
        . "ON DUPLICATE KEY UPDATE $assignments";
    }
}